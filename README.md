# Lip Sync Code Online Version

The code used for lip-syncing is given in ```dubbing_code.py```. The same code with comments generated is given in ```dubbing_code_with_comments.py```. The function that does lip-syncing in the code is ```change_video_audio```. Please look into the fuction to understand how lip-syncing is done.
